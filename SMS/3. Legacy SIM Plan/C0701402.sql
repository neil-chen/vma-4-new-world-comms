/*
Legacy $25/$30 SIM Plan customers
Plan Mapping: Good/Better/Best offer
Offer type: 12M SIM Only Offer
CampaignID: C0701402
*/
SELECT 
    NEWID() AS id,
    DateAdd(hh,11, getUTCdate()) AS createdate,
    A.SMSCampaignIDFlag AS mcid,
    A.original_cellular_no,
    A.service_number,
    A.firstname AS customer_name,
    Convert(varchar(30),DateAdd(hh,11, getUTCdate()),112) + '020000' AS sms_date,
    A.saville_account_number,
    CASE
        /* Apple */
        WHEN A.handset_brand = 'Apple' THEN
            (CASE
                /* M18-19 Good Offer */
                WHEN A.contract_tenure = '18' OR A.contract_tenure = '19' THEN 
                    (CASE
                        /* Bonus Data */
                        WHEN A.handset_1_other_bolton_jarvis != '0' AND (ISNULL(A.handset_1_other_bolton_jarvis,'') <> '') THEN
                            ('Hi ' + A.firstname + ', get a 12 month SIM Only plan for $' + A.handset_1_optus_access + ' a month with ' + A.handset_1_optus_allowance + 'GB of data + ' + A.handset_1_other_bolton_jarvis + 'GB bonus data for 24 mths, min total cost over 12 months is $' + A.hdst_1_plan_min_total_cost + '. PLUS we''ll waive the remaining months of your current contract. So what are you waiting for? SWITCH TO OPTUS TODAY! ' + D.sms_url + ' Contact us 136369. Unsub at virg.in/unsub')
                        /* No Bonus Data */
                        ELSE
                            ('Hi ' + A.firstname + ', get a 12 month SIM Only plan for $' + A.handset_1_optus_access + ' a month with ' + A.handset_1_optus_allowance + 'GB of data, min total cost over 12 months is $' + A.hdst_1_plan_min_total_cost + '. PLUS we''ll waive the remaining months of your current contract. So what are you waiting for? SWITCH TO OPTUS TODAY! ' + D.sms_url + ' Contact us 136369. Unsub at virg.in/unsub')
                    END)
                /* M20 Better Offer */
                WHEN A.contract_tenure = '20' THEN 
                    (CASE
                        /* Bonus Data */
                        WHEN A.bet_hdst_1_other_bolton_jarvis != '0' AND (ISNULL(A.bet_hdst_1_other_bolton_jarvis,'') <> '') THEN
                            ('Hi ' + A.firstname + ', get a 12 month SIM Only plan for $' + A.bet_hdst_1_optus_access + ' a month with ' + A.bet_hdst_1_optus_allowance + 'GB of data + ' + A.bet_hdst_1_other_bolton_jarvis + 'GB bonus data for 24 mths, min total cost over 12 months is $' + A.bet_hdst_1_plan_min_total_cost + '. PLUS we''ll waive the remaining months of your current contract. So what are you waiting for? SWITCH TO OPTUS TODAY! ' + D.sms_url + ' Contact us 136369. Unsub at virg.in/unsub')
                        /* No Bonus Data */
                        ELSE
                            ('Hi ' + A.firstname + ', get a 12 month SIM Only plan for $' + A.bet_hdst_1_optus_access + ' a month with ' + A.bet_hdst_1_optus_allowance + 'GB of data, min total cost over 12 months is $' + A.bet_hdst_1_plan_min_total_cost + '. PLUS we''ll waive the remaining months of your current contract. So what are you waiting for? SWITCH TO OPTUS TODAY! ' + D.sms_url + ' Contact us 136369. Unsub at virg.in/unsub')
                    END)
                /* M22-24 Best Offer */
                WHEN A.contract_tenure = '22' OR A.contract_tenure = '23' OR A.contract_tenure = '24' THEN 
                    (CASE
                        /* Bonus Data */
                        WHEN A.bst_hdst_1_other_bolton_jarvis != '0' AND (ISNULL(A.bst_hdst_1_other_bolton_jarvis,'') <> '') THEN
                            ('Hi ' + A.firstname + ', get a 12 month SIM Only plan for $' + A.bst_hdst_1_optus_access + ' a month with ' + A.bst_hdst_1_optus_allowance + 'GB of data + ' + A.bst_hdst_1_other_bolton_jarvis + 'GB bonus data for 24 mths, min total cost over 12 months is $' + A.bst_hdst_1_plan_min_total_cost + '. PLUS we''ll waive the remaining months of your current contract. So what are you waiting for? SWITCH TO OPTUS TODAY! ' + D.sms_url + ' Contact us 136369. Unsub at virg.in/unsub')
                        /* No Bonus Data */
                        ELSE
                            ('Hi ' + A.firstname + ', get a 12 month SIM Only plan for $' + A.bst_hdst_1_optus_access + ' a month with ' + A.bst_hdst_1_optus_allowance + 'GB of data, min total cost over 12 months is $' + A.bst_hdst_1_plan_min_total_cost + '. PLUS we''ll waive the remaining months of your current contract. So what are you waiting for? SWITCH TO OPTUS TODAY! ' + D.sms_url + ' Contact us 136369. Unsub at virg.in/unsub')
                    END)
                /* Accelerated + SIMOnly */
                ELSE
                    (CASE
                        /* Bonus Data */
                        WHEN A.bst_hdst_1_other_bolton_jarvis != '0' AND (ISNULL(A.bst_hdst_1_other_bolton_jarvis,'') <> '') THEN
                            ('Hi ' + A.firstname + ', get a 12 month SIM Only plan for $' + A.bst_hdst_1_optus_access + ' a month with ' + A.bst_hdst_1_optus_allowance + 'GB of data + ' + A.bst_hdst_1_other_bolton_jarvis + 'GB bonus data for 24 mths, min total cost over 12 months is $' + A.bst_hdst_1_plan_min_total_cost + '. So what are you waiting for? SWITCH TO OPTUS TODAY! ' + D.sms_url + ' Contact us 136369. Unsub at virg.in/unsub')
                        /* No Bonus Data */
                        ELSE
                            ('Hi ' + A.firstname + ', get a 12 month SIM Only plan for $' + A.bst_hdst_1_optus_access + ' a month with ' + A.bst_hdst_1_optus_allowance + 'GB of data, min total cost over 12 months is $' + A.bst_hdst_1_plan_min_total_cost + '. So what are you waiting for? SWITCH TO OPTUS TODAY! ' + D.sms_url + ' Contact us 136369. Unsub at virg.in/unsub')
                    END)
            END)
        /* Non-Apple */
        WHEN A.handset_brand = 'Non-Apple' THEN
            (CASE
                /* M18-19 Good Offer */
                WHEN A.contract_tenure = '18' OR A.contract_tenure = '19' THEN 
                    (CASE
                        /* Bonus Data */
                        WHEN A.handset_2_other_bolton_jarvis != '0' AND (ISNULL(A.handset_2_other_bolton_jarvis,'') <> '') THEN
                            ('Hi ' + A.firstname + ', get a 12 month SIM Only plan for $' + A.handset_2_optus_access + ' a month with ' + A.handset_2_optus_allowance + 'GB of data + ' + A.handset_2_other_bolton_jarvis + 'GB bonus data for 24 mths, min total cost over 12 months is $' + A.hdst_2_plan_min_total_cost + '. PLUS we''ll waive the remaining months of your current contract. So what are you waiting for? SWITCH TO OPTUS TODAY! ' + D.sms_url + ' Contact us 136369. Unsub at virg.in/unsub')
                        /* No Bonus Data */
                        ELSE
                            ('Hi ' + A.firstname + ', get a 12 month SIM Only plan for $' + A.handset_2_optus_access + ' a month with ' + A.handset_2_optus_allowance + 'GB of data, min total cost over 12 months is $' + A.hdst_2_plan_min_total_cost + '. PLUS we''ll waive the remaining months of your current contract. So what are you waiting for? SWITCH TO OPTUS TODAY! ' + D.sms_url + ' Contact us 136369. Unsub at virg.in/unsub')
                    END)
                /* M20 Better Offer */
                WHEN A.contract_tenure = '20' THEN 
                    (CASE
                        /* Bonus Data */
                        WHEN A.bet_hdst_2_other_bolton_jarvis != '0' AND (ISNULL(A.bet_hdst_2_other_bolton_jarvis,'') <> '') THEN
                            ('Hi ' + A.firstname + ', get a 12 month SIM Only plan for $' + A.bet_hdst_2_optus_access + ' a month with ' + A.bet_hdst_2_optus_allowance + 'GB of data + ' + A.bet_hdst_2_other_bolton_jarvis + 'GB bonus data for 24 mths, min total cost over 12 months is $' + A.bet_hdst_2_plan_min_total_cost + '. PLUS we''ll waive the remaining months of your current contract. So what are you waiting for? SWITCH TO OPTUS TODAY! ' + D.sms_url + ' Contact us 136369. Unsub at virg.in/unsub')
                        /* No Bonus Data */
                        ELSE
                            ('Hi ' + A.firstname + ', get a 12 month SIM Only plan for $' + A.bet_hdst_2_optus_access + ' a month with ' + A.bet_hdst_2_optus_allowance + 'GB of data, min total cost over 12 months is $' + A.bet_hdst_2_plan_min_total_cost + '. PLUS we''ll waive the remaining months of your current contract. So what are you waiting for? SWITCH TO OPTUS TODAY! ' + D.sms_url + ' Contact us 136369. Unsub at virg.in/unsub')
                    END)
                /* M22-24 Best Offer */
                WHEN A.contract_tenure = '22' OR A.contract_tenure = '23' OR A.contract_tenure = '24' THEN 
                    (CASE
                        /* Bonus Data */
                        WHEN A.bst_hdst_2_other_bolton_jarvis != '0' AND (ISNULL(A.bst_hdst_2_other_bolton_jarvis,'') <> '') THEN
                            ('Hi ' + A.firstname + ', get a 12 month SIM Only plan for $' + A.bst_hdst_2_optus_access + ' a month with ' + A.bst_hdst_2_optus_allowance + 'GB of data + ' + A.bst_hdst_2_other_bolton_jarvis + 'GB bonus data for 24 mths, min total cost over 12 months is $' + A.bst_hdst_2_plan_min_total_cost + '. PLUS we''ll waive the remaining months of your current contract. So what are you waiting for? SWITCH TO OPTUS TODAY! ' + D.sms_url + ' Contact us 136369. Unsub at virg.in/unsub')
                        /* No Bonus Data */
                        ELSE
                            ('Hi ' + A.firstname + ', get a 12 month SIM Only plan for $' + A.bst_hdst_2_optus_access + ' a month with ' + A.bst_hdst_2_optus_allowance + 'GB of data, min total cost over 12 months is $' + A.bst_hdst_2_plan_min_total_cost + '. PLUS we''ll waive the remaining months of your current contract. So what are you waiting for? SWITCH TO OPTUS TODAY! ' + D.sms_url + ' Contact us 136369. Unsub at virg.in/unsub')
                    END)
                /* Accelerated + SIMOnly */
                ELSE
                    (CASE
                        /* Bonus Data */
                        WHEN A.bst_hdst_2_other_bolton_jarvis != '0' AND (ISNULL(A.bst_hdst_2_other_bolton_jarvis,'') <> '') THEN
                            ('Hi ' + A.firstname + ', get a 12 month SIM Only plan for $' + A.bst_hdst_2_optus_access + ' a month with ' + A.bst_hdst_2_optus_allowance + 'GB of data + ' + A.bst_hdst_2_other_bolton_jarvis + 'GB bonus data for 24 mths, min total cost over 12 months is $' + A.bst_hdst_2_plan_min_total_cost + '. So what are you waiting for? SWITCH TO OPTUS TODAY! ' + D.sms_url + ' Contact us 136369. Unsub at virg.in/unsub')
                        /* No Bonus Data */
                        ELSE
                            ('Hi ' + A.firstname + ', get a 12 month SIM Only plan for $' + A.bst_hdst_2_optus_access + ' a month with ' + A.bst_hdst_2_optus_allowance + 'GB of data, min total cost over 12 months is $' + A.bst_hdst_2_plan_min_total_cost + '. So what are you waiting for? SWITCH TO OPTUS TODAY! ' + D.sms_url + ' Contact us 136369. Unsub at virg.in/unsub')
                    END)
            END)
    END AS sms_text
FROM
    [Legacy_SIM_Plan_Jan_EntryDE] A
    LEFT JOIN [EXCLUDE_SYSTEM_MIGRATION] C ON A.original_cellular_no = C.original_cellular_no
    LEFT JOIN [Legacy_SIM_Plan_SMS_URL_Mapping] D ON A.cluster_id = D.cluster_id AND A.SMSCampaignIDFlag = D.campaign_id
WHERE
    A.SMSCampaignIDFlag = 'C0701402'
    AND ISNULL(A.firstname,'') <> ''
    AND ISNULL(C.original_cellular_no,'') = ''
    AND ISNULL(A.handset_brand,'') <> ''
    AND (
        
                (A.handset_brand = 'Apple' AND (A.contract_tenure = '18' OR A.contract_tenure = '19') AND 
            (
                ISNULL(A.handset_1_optus_allowance,'') <> '' AND A.handset_1_optus_allowance != '0'
                AND ISNULL(A.handset_1_optus_access,'') <> '' AND A.handset_1_optus_access != '0'
                AND ISNULL(A.handset_1_contract_period,'') <> '' AND A.handset_1_contract_period != '0'
                AND ISNULL(A.hdst_1_plan_min_total_cost,'') <> '' AND A.hdst_1_plan_min_total_cost != '0'
                AND A.handset_1_legacy_rate_plan_cod = '12m Sim Only'
            )
                )
            OR  (A.handset_brand = 'Apple' AND A.contract_tenure = '20' AND 
            (
                ISNULL(A.bet_hdst_1_optus_allowance,'') <> '' AND A.bet_hdst_1_optus_allowance != '0'
                AND ISNULL(A.bet_hdst_1_optus_access,'') <> '' AND A.bet_hdst_1_optus_access != '0'
                AND ISNULL(A.bet_hdst_1_contract_period,'') <> '' AND A.bet_hdst_1_contract_period != '0'
                AND ISNULL(A.bet_hdst_1_plan_min_total_cost,'') <> '' AND A.bet_hdst_1_plan_min_total_cost != '0'
                AND A.bet_hdst_1_legacy_rate_plan_cd = '12m Sim Only'
            )
                )
            OR  (A.handset_brand = 'Apple' AND (A.contract_tenure <> '18' AND A.contract_tenure <> '19' AND A.contract_tenure <> '20') AND 
            (
                ISNULL(A.bst_hdst_1_optus_allowance,'') <> '' AND A.bst_hdst_1_optus_allowance != '0'
                AND ISNULL(A.bst_hdst_1_optus_access,'') <> '' AND A.bst_hdst_1_optus_access != '0'
                AND ISNULL(A.bst_hdst_1_contract_period,'') <> '' AND A.bst_hdst_1_contract_period != '0'
                AND ISNULL(A.bst_hdst_1_plan_min_total_cost,'') <> '' AND A.bst_hdst_1_plan_min_total_cost != '0'
                AND A.bst_hdst_1_legacy_rate_plan_cd = '12m Sim Only'
            )
                )
            OR  (A.handset_brand = 'Non-Apple' AND (A.contract_tenure = '18' OR A.contract_tenure = '19') AND 
            (
                ISNULL(A.handset_2_optus_allowance,'') <> '' AND A.handset_2_optus_allowance != '0'
                AND ISNULL(A.handset_2_optus_access,'') <> '' AND A.handset_2_optus_access != '0'
                AND ISNULL(A.handset_2_contract_period,'') <> '' AND A.handset_2_contract_period != '0'
                AND ISNULL(A.hdst_2_plan_min_total_cost,'') <> '' AND A.hdst_2_plan_min_total_cost != '0'
                AND A.handset_2_legacy_rate_plan_cod = '12m Sim Only'
            )
                )
            OR  (A.handset_brand = 'Non-Apple' AND A.contract_tenure = '20' AND 
            (
                ISNULL(A.bet_hdst_2_optus_allowance,'') <> '' AND A.bet_hdst_2_optus_allowance != '0'
                AND ISNULL(A.bet_hdst_2_optus_access,'') <> '' AND A.bet_hdst_2_optus_access != '0'
                AND ISNULL(A.bet_hdst_2_contract_period,'') <> '' AND A.bet_hdst_2_contract_period != '0'
                AND ISNULL(A.bet_hdst_2_plan_min_total_cost,'') <> '' AND A.bet_hdst_2_plan_min_total_cost != '0'
                AND A.bet_hdst_2_legacy_rate_plan_cd = '12m Sim Only'
            )
                )
            OR  (A.handset_brand = 'Non-Apple' AND (A.contract_tenure <> '18' AND A.contract_tenure <> '19' AND A.contract_tenure <> '20') AND 
            (
                ISNULL(A.bst_hdst_2_optus_allowance,'') <> '' AND A.bst_hdst_2_optus_allowance != '0'
                AND ISNULL(A.bst_hdst_2_optus_access,'') <> '' AND A.bst_hdst_2_optus_access != '0'
                AND ISNULL(A.bst_hdst_2_contract_period,'') <> '' AND A.bst_hdst_2_contract_period != '0'
                AND ISNULL(A.bst_hdst_2_plan_min_total_cost,'') <> '' AND A.bst_hdst_2_plan_min_total_cost != '0'
                AND A.bst_hdst_2_legacy_rate_plan_cd = '12m Sim Only'
            )
                )

        )   
    