/*
Tenure Month: M17
CampaignID: C0701102
*/
SELECT 
    NEWID() AS id,
    DateAdd(hh,11, getUTCdate()) AS createdate,
    A.SMSCampaignIDFlag AS mcid,
    A.original_cellular_no,
    A.service_number,
    A.firstname AS customer_name,
    Convert(varchar(30),DateAdd(hh,11, getUTCdate()),112) + '020000' AS sms_date,
    A.saville_account_number,
    ('Hi ' + firstname + ', with the Virgin Mobile brand being phased out of Australia next year, we''ve been busy working with our friends at Optus on some amazing offers for you. You don''t need to do anything now but keep your eyes peeled, we''ll be in touch soon with some awesome offers that you won''t want to miss out on. Contact us 136369. Unsub at virg.in/unsub')
    AS sms_text
FROM
    [M17_TenureBasedJourney_Dec_EntryDE] A
    LEFT JOIN [CustomerLifeCycle_TenureBasedJourney_SMS] B ON A.original_cellular_no = B.original_cellular_no AND A.SMSCampaignIDFlag = B.mcid
    LEFT JOIN [EXCLUDE_SYSTEM_MIGRATION] C ON A.original_cellular_no = C.original_cellular_no
WHERE
    A.SMSCampaignIDFlag = 'C0701102'
    AND ISNULL(A.firstname,'') <> ''
    AND ISNULL(B.original_cellular_no,'') = ''
    AND ISNULL(C.original_cellular_no,'') = ''
