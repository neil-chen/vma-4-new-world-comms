/*
Accelerated Month: 10 Follow Up
Plan Mapping: Best offer
Offer type: SIM Only Plan
CampaignID: C0703036
Bonus Data: bst_hdst_1_other_bolton_jarvis
Apple Music: bst_hdst_1_other_bolton_legacy
*/
SELECT 
    NEWID() AS id,
    DateAdd(hh,11, getUTCdate()) AS createdate,
    'C0703036' AS mcid,
    A.original_cellular_no,
    A.service_number,
    A.firstname AS customer_name,
    Convert(varchar(30),DateAdd(hh,11, getUTCdate()),112) + '020000' AS sms_date,
    A.saville_account_number,
    'Hi ' + A.firstname + ', enjoy 3 months free plan fees when you switch to Optus & sign up to a new selected mobile plan. Not available with any other discount offers. Cancellation fees may apply. Ends 24/11/19 unless withdrawn earlier. T&Cs apply. Switch to Optus today @ ' + D.sms_url + ' Contact us 136369. Unsub at virg.in/unsub.' AS sms_text
FROM
    [CustomerLifeCycle_AcceleratedJourney_Dec_EntryDE] A
    LEFT JOIN [CustomerLifeCycle_AcceleratedJourney_SMS] B ON A.original_cellular_no = B.original_cellular_no AND B.mcid = 'C0703036'
    LEFT JOIN [EXCLUDE_SYSTEM_MIGRATION] C ON A.original_cellular_no = C.original_cellular_no
    LEFT JOIN [AcceleratedJourney_SMS_URL_Mapping] D ON A.cluster_id = D.cluster_id AND D.campaign_id = 'C0703036'
    LEFT JOIN [DT_DLY_SERVICE_SEGMENTATION] E ON A.original_cellular_no = E.original_cellular_no AND E.sms_permission = 'True'
WHERE
    ISNULL(A.firstname,'') <> ''
    AND ISNULL(B.original_cellular_no,'') = ''
    AND ISNULL(C.original_cellular_no,'') = ''
    AND ISNULL(E.original_cellular_no,'') <> ''