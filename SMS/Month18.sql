/*
Tenure Month: M18
Plan Mapping: Good offer
Offer type: M2M / M2M + Handset
CampaignID: C0701104
Bonus Data: handset_1_other_bolton_jarvis
Apple Music: handset_1_other_bolton_legacy
*/
SELECT 
    NEWID() AS id,
    DateAdd(hh,11, getUTCdate()) AS createdate,
    A.SMSCampaignIDFlag AS mcid,
    A.original_cellular_no,
    A.service_number,
    A.firstname AS customer_name,
    Convert(varchar(30),DateAdd(hh,11, getUTCdate()),112) + '020000' AS sms_date,
    A.saville_account_number,
    CASE
        /* Apple */
        WHEN A.handset_brand = 'Apple' THEN
            (CASE
                /* M2M with handset */
                WHEN A.handset_1_legacy_rate_plan_cod = 'M2M with handset' THEN 
                    (CASE
                        /* Bonus Data */
                        WHEN A.handset_1_other_bolton_jarvis != '0' AND (ISNULL(A.handset_1_other_bolton_jarvis,'') <> '') THEN
                            ()
                        /* No Bonus Data */
                        ELSE
                            ()
                    END)
                /* M2M without handset */
                WHEN A.handset_1_legacy_rate_plan_cod = 'M2M without handset' THEN 
                    (CASE
                        /* Bonus Data */
                        WHEN A.handset_1_other_bolton_jarvis != '0' AND (ISNULL(A.handset_1_other_bolton_jarvis,'') <> '') THEN
                            ()
                        /* No Bonus Data */
                        ELSE
                            ()
                    END)
            END)
        /* Non-Apple */
        WHEN A.handset_brand = 'Non-Apple' THEN
            (CASE
                /* M2M with handset */
                WHEN A.handset_2_legacy_rate_plan_cod = 'M2M with handset' THEN 
                    (CASE
                        /* Bonus Data */
                        WHEN A.handset_2_other_bolton_jarvis != '0' AND (ISNULL(A.handset_2_other_bolton_jarvis,'') <> '') THEN
                            ()
                        /* No Bonus Data */
                        ELSE
                            ()
                    END)
                /* M2M without handset */
                WHEN A.handset_2_legacy_rate_plan_cod = 'M2M without handset' THEN 
                    (CASE
                        /* Bonus Data */
                        WHEN A.handset_2_other_bolton_jarvis != '0' AND (ISNULL(A.handset_2_other_bolton_jarvis,'') <> '') THEN
                            ()
                        /* No Bonus Data */
                        ELSE
                            ()
                    END)
            END)
    END AS sms_text
FROM
    [M18_TenureBasedJourney_Dec_EntryDE] A
    LEFT JOIN [CustomerLifeCycle_TenureBasedJourney_SMS] B ON A.original_cellular_no = B.original_cellular_no AND A.SMSCampaignIDFlag = B.mcid
    LEFT JOIN [EXCLUDE_SYSTEM_MIGRATION] C ON A.original_cellular_no = C.original_cellular_no
    LEFT JOIN [TenureBasedJourney_SMS_URL_Mapping] D ON A.cluster_id = D.cluster_id AND A.SMSCampaignIDFlag = D.campaign_id
WHERE
    A.SMSCampaignIDFlag = 'C0701104'
    AND ISNULL(A.firstname,'') <> ''
    AND ISNULL(B.original_cellular_no,'') = ''
    AND ISNULL(C.original_cellular_no,'') = ''
    AND ISNULL(A.handset_brand,'') <> ''
    AND (
        
                (A.handset_brand = 'Apple' AND A.handset_1_legacy_rate_plan_cod = 'M2M with handset' AND 
            (
                ISNULL(A.handset_1,'') <> '' AND A.handset_1 != '0'
                AND ISNULL(A.handset_1_storage,'') <> '' AND A.handset_1_storage != '0'
                AND ISNULL(A.hdst_1_monthly_total_cost,'') <> '' AND A.hdst_1_monthly_total_cost != '0'
                AND ISNULL(A.handset_1_optus_allowance,'') <> '' AND A.handset_1_optus_allowance != '0'
                AND ISNULL(A.hdst_1_hand_min_total_cost,'') <> '' AND A.hdst_1_hand_min_total_cost != '0'
                AND ISNULL(A.handset_1_contract_period,'') <> '' AND A.handset_1_contract_period != '0'
            )
                )
            OR  (A.handset_brand = 'Apple' AND A.handset_1_legacy_rate_plan_cod = 'M2M without handset' AND 
            (
                ISNULL(A.handset_1_optus_access,'') <> '' AND A.handset_1_optus_access != '0'
                AND ISNULL(A.handset_1_optus_allowance,'') <> '' AND A.handset_1_optus_allowance != '0'
                AND ISNULL(A.handset_1_contract_period,'') <> '' AND A.handset_1_contract_period != '0' 
            )
            OR  (A.handset_brand = 'Non-Apple' AND A.handset_2_legacy_rate_plan_cod = 'M2M with handset' AND 
            (
                ISNULL(A.handset_2,'') <> '' AND A.handset_2 != '0'
                AND ISNULL(A.handset_2_storage,'') <> '' AND A.handset_2_storage != '0'
                AND ISNULL(A.hdst_2_monthly_total_cost,'') <> '' AND A.hdst_2_monthly_total_cost != '0'
                AND ISNULL(A.handset_2_optus_allowance,'') <> '' AND A.handset_2_optus_allowance != '0'
                AND ISNULL(A.hdst_2_hand_min_total_cost,'') <> '' AND A.hdst_2_hand_min_total_cost != '0'
                AND ISNULL(A.handset_2_contract_period,'') <> '' AND A.handset_2_contract_period != '0'
            )
                )
            OR  (A.handset_brand = 'Non-Apple' AND A.handset_2_legacy_rate_plan_cod = 'M2M without handset' AND 
            (
                ISNULL(A.handset_2_optus_access,'') <> '' AND A.handset_2_optus_access != '0'
                AND ISNULL(A.handset_2_optus_allowance,'') <> '' AND A.handset_2_optus_allowance != '0'
                AND ISNULL(A.handset_2_contract_period,'') <> '' AND A.handset_2_contract_period != '0' 
            )
                )
        )   
    